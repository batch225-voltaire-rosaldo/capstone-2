// #region  Product Controller
const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productController");

// Create Product
router.post("/addProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.addProduct(data).then(result => res.send(result));
});

router.get("/", (req, res) => { 
	productController.getAllProducts().then(result => res.send(result));
});

// Get All Active Product
router.get("/getAllActive", auth.verify, (req, res) => {
	productController.getAllActive().then(result => res.send(result));
});

// Get all products
// router.get("/getAllProducts", (req, res) => { //need for middleware
// 	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
// });

// Get Product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(result => res.send(result));
});



router.post("/getProduct", (req, res) => {
	productController.getProduct({name : req.body.name}).then(resultFromController => res.send(resultFromController));
});

// Update Product Information
router.put("/updateProduct/:productId", auth.verify, (req, res) => {
	courseController.updateProduct(req.params, req.body).then(result => res.send(result));
});

// Archive Product
router.put("/archive/:productId", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(result => res.send(result));
});

// Activate Product
router.put("/unarchive/:productId", auth.verify, (req, res) => {
	productController.unarchiveProduct(req.params).then(result => res.send(result));
});

module.exports = router;
// #endregion
