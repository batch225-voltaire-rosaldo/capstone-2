// #region Product Schema
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product Name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},

	//productQuantity : {
		//type : Number,
		//default : 1
	//},
	
	// image: {
	//   type: String,
	//   required: [true, "Link image required"]
	// },

	isActive : {
		type : Boolean,
		default : true
	},
	onStock : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrder : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			//productQuantity : {
				//type : Number
				//required : [true, "Quantity is required"]
			//},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);
// #endregion
