const Product = require("../models/product");

// Create Product 
module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});
		return new_product.save().then((new_product, error) => {
			if(error){
				return error
			}
			return {
				message: 'New product successfully created!'
			}
		});
	} else {
	let message = Promise.resolve('User must me Admin to access this.')
	return message.then((value) => {
		return value
		});
	};
};

// Get all Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// Get all Products
module.exports.getAllProducts = (data) => {
	return Product.find({}).then(result => {
		return result;
	});
};
// Get Product
module.exports.getProduct= (data) => {

	return Product.findById(data.productId).then(result => {
		return result;
	}) ;
}

// module.exports.getProduct = (reqBody) => {
// 	return Product.find({name : reqBody.name, isActive : true}).then(result => {
// 		if (result.length > 1) {
// 			return {
// 				message: "Duplicate Product"
// 			} 
// 		} else if (result.length > 0){
// 			return result //{ message: "Product found"}
// 		} else {
// 			return {
// 				message: "Product not found"
// 			}
// 		}
// 	});
// };

// Update Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price : reqBody.price,
		isActive : reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "Product updated successfully"
			}
		};
	});
};

// Archive Product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "Archiving product successfully"
			}
		};
	});
};

// Activate Product
module.exports.unarchiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : true
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "Activate Product successfully"
			}
		};
	});
};
