const User = require("../models/user");
const Product = require("../models/product");
//const Order = require("../models/order");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return user
		};
	});
}; 

// User Login
module.exports.loginUser = (data) => {
	return User.findOne({email : data.email}).then(result => {
		if(result == null ){
			return {
				message: "User not found in database."
			}
		} else {
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);
			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return {
					message: "Password was incorrect."
				}
			};
		};
	});
};

// Check Email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 1) {
			return {
				message: "Duplicate User"
			} 
		} else if (result.length > 0){
			return {
				message: "User was found"
			}
		} else {
			return {
				message: "User not found"
			}
		}
	});
};

// Set User as Admin or just User
module.exports.updateUsertoAdmin = (reqParams) => {
	let updatedUser = {
		isAdmin : true
	};
	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "User changed to Admin successfully"
			}
		};
	});
};

// Get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};

// Retrieve User Details
// module.exports.getDetail = (data) => {
// 	return User.findById(data.userId).then(result => {
// 		result.password = "";
// 		return result;
// 	});
// };

module.exports.getProfile = (userData) => {
	return User.findOne({id : userData.id}).then(result => {
		if (result == null){
			return false
		} else {
			result.password = ""
			return result
		}
	});
};

// Order
module.exports.order = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.order.push({productId : data.productId});
		//user.order.push({productQuantity : data.productQuantity});

		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.userOrder.push({userId : data.userId});
		//product.userOrder.push({productId : data.productId});
		//product.userOrder.push({productQuantity : data.productQuantity});
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	if(isUserUpdated && isProductUpdated){
		return {
			message: "Order Added"
		}
	} else {
		return false;
	};
};
