// #region JWT Secret
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv").config();

const secret = `${process.env.SECRET}`;
// #endregion

// #region Create Access Token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}
	return jwt.sign(data, secret, {
		//expiresIn: "10d" // - for session timeout
	});
};
// #endregion

// #region Verify Token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth : "Verification failed."})
			} else {
				next()	
			}
		})
	} else {
		return res.send({auth : "Authentication failed."});
	};
};
// #endregion

// #region Decode Token
module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete:true}).payload;
			}	
		})
	} else {
		return null;
	};
};
// #endregion
